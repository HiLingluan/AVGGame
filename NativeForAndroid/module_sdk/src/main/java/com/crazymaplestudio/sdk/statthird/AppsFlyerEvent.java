package com.crazymaplestudio.sdk.statthird;

/**
 * description
 * Created by jin
 * Created date: 2021-08-18
 */
public class AppsFlyerEvent {

    //点击封面事件
    public static final String  AF_BOOK_COVER_CLICK         = "af_book_cover_click";
    //完成章节事件
    public static final String  AF_CHAP_COMPLETE            = "af_chap_complete";
    //开始章节事件
    public static final String  AF_CHAP_START               = "af_chap_start";
    //选择选项事件
    public static final String  AF_CHOICE                   = "af_choice";
    //选择付费选项事件
    public static final String  AF_PREMIUM_CHOICE           = "af_premium_choice";
    //选择付费服装事件
    public static final String  AF_PREMIUM_OUTFIT           = "af_premium_outfit";
    //捏脸事件
    public static final String  AF_MAKEUP                   = "af_makeup";
    //消耗钻石事件
    public static final String  AF_DIAMOND_CONSUME          = "af_diamond_consume";
    //加入购物车事件
    public static final String  AF_ADD_TO_CART              = "af_add_to_cart";
    //登录完成事件
    public static final String  AF_COMPLETE_REGISTRATION    = "af_complete_registration";
    //评论事件
    public static final String  AF_COMMENT                  = "af_comment";
    //付费事件 这个应该用appsflyer默认的
    public static final String  AF_PURCHASE_COMPLETE        = "af_purchase_complete";
    //观看广告事件
    public static final String  AF_WATCH_VIDEO              = "af_watch_video";
    //完成积分墙任务事件
    public static final String  AF_COMPLETE_OFFER           = "af_complete_offer";
    //新用户登录两次
    public static final String  AF_NEWUSER_SISSION_2        = "af_newuser_sission_2";
    //新用户首日付费
    public static final String  AF_NEWUSER_PURCHASE_DAY1    = "af_newuser_purchase_day1";
    //新用户读完1个章节
    public static final String  AF_NEWUSER_C_CHAP_1         = "af_newuser_c_chap_1";
    //新用户读完2个章节
    public static final String  AF_NEWUSER_C_CHAP_2         = "af_newuser_c_chap_2";
    //新用户读完3个章节
    public static final String  AF_NEWUSER_C_CHAP_3         = "af_newuser_c_chap_3";
    //新用户读完5个章节
    public static final String  AF_NEWUSER_C_CHAP_5         = "af_newuser_c_chap_5";
    //新用户读完7个章节
    public static final String  AF_NEWUSER_C_CHAP_7         = "af_newuser_c_chap_7";
    //新用户读完10个章节
    public static final String  AF_NEWUSER_C_CHAP_10        = "af_newuser_c_chap_10";
    //新用户读完15个章节
    public static final String  AF_NEWUSER_C_CHAP_15        = "af_newuser_c_chap_15";
    //新用户读完1本书
    public static final String  AF_NEWUSER_C_BOOK_1        = "af_newuser_c_book_1";
    //新用户读完2本书
    public static final String  AF_NEWUSER_C_BOOK_2        = "af_newuser_c_book_2";
    //新用户读完3本书
    public static final String  AF_NEWUSER_C_BOOK_3        = "af_newuser_c_book_3";
    //新用户读完5本书
    public static final String  AF_NEWUSER_C_BOOK_5        = "af_newuser_c_book_5";
    //新用户读完7本书
    public static final String  AF_NEWUSER_C_BOOK_7        = "af_newuser_c_book_7";
    //新用户读完10本书
    public static final String  AF_NEWUSER_C_BOOK_10        = "af_newuser_c_book_10";


}
