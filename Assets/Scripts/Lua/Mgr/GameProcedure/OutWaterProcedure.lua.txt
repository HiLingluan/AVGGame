-- des:{倒水流程}
-- author:{zpf}
-- date: 2021-12-16 09:33:00
--
local ProcedureBase = require "Mgr.GameProcedure.ProcedureBase"
local OutWaterProcedure = class("OutWaterProcedure",ProcedureBase)

function OutWaterProcedure:ctor()
    OutWaterProcedure.base(self).ctor(self, Define.EGameProcedure.OutWater)
    
end

function OutWaterProcedure:onInit()
    self.super.onInit(self)
    
end

function OutWaterProcedure:onEnter()
    self.super.onEnter(self)
    self.fsm:setData("LastProcedure","OutWaterProcedure")
    local sceneMgr = Context.GetApplicationContext():GetService("SceneMgr")
    sceneMgr:LoadSceneAsync("watersort",CS.UnityEngine.SceneManagement.LoadSceneMode.Single,function (sceneName)
        self:onSceneLoad(sceneName)
    end)
end

function OutWaterProcedure:onUpdate()
    
end

function OutWaterProcedure:onLeave()
    self.super.onLeave(self)
    ViewMgr:DestroyWindow(EWindowType.WaterPalace)
    ViewMgr:DestroyWindow(EWindowType.WaterTube)
end

function OutWaterProcedure:onDestroy()
    self.super.onDestroy(self)
    
end

function OutWaterProcedure:onSceneLoad(sceneName)
    
end

return OutWaterProcedure