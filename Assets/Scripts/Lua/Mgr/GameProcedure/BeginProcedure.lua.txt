-- des:{开始流程}
-- author:{zpf}
-- date: 2021-12-16 09:33:00
--
local ProcedureBase = require "Mgr.GameProcedure.ProcedureBase"
local BeginProcedure = class("BeginProcedure",ProcedureBase)

function BeginProcedure:ctor()
    BeginProcedure.base(self).ctor(self, Define.EGameProcedure.Begin)
    
end

function BeginProcedure:onInit()
    self.super.onInit(self)
end

function BeginProcedure:onEnter()
    self.super.onEnter(self)
    self.fsm:setData("isFromGame",false)
    ViewMgr:DestroyAllPanel()
    GameEntry.Timer:RemoveAllCallBacks()
    SoundMgr:StopAll()
    CS.DG.Tweening.DOTween.KillAll()
    GameEntry.OutAsset:Clear()
    Util.CSDispatcher(CSEventKey.ENTER_BEGIN_SCENE)
    
    StatUtil.setCurScene(StatConstant.SCENE.Launch)
end

function BeginProcedure:onUpdate()
    self.super.onUpdate(self)

end

function BeginProcedure:onLeave()
    self.super.onLeave(self)
    
end

function BeginProcedure:onDestroy()
    self.super.onDestroy(self)

end

return BeginProcedure