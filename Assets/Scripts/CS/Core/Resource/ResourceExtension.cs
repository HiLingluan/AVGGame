﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using GameFramework;
using GameFramework.Resource;
using UnityEngine;
using UnityGameFramework.Runtime;


public static class ResourceExtension
{   
    
    private const string DefaultExtension = "dat";
    public static void MoveFileSystemToWrite(this ResourceComponent resourceComponent, string fileSystemName,Action<string> callBack)
    {
        string fullWritePath = Utility.Path.GetRegularPath(Path.Combine(resourceComponent.ReadWritePath, Utility.Text.Format("{0}.{1}", fileSystemName, DefaultExtension)));
        string fullReadPath = Utility.Path.GetRegularPath(Path.Combine(resourceComponent.ReadOnlyPath, Utility.Text.Format("{0}.{1}", fileSystemName, DefaultExtension)));

        if(File.Exists(fullWritePath)){
            callBack?.Invoke(fullWritePath);
            return;
        }
        int downloadId =  GameEntry.Download.AddDownload(fullWritePath,fullReadPath);
        GameEntry.Event.Subscribe(DownloadSuccessEventArgs.EventId, (sender, eventArgs) =>
        {
            DownloadSuccessEventArgs downloadSuccessEventArgs = eventArgs as DownloadSuccessEventArgs;
            if (downloadSuccessEventArgs.SerialId == downloadId)
            {
                callBack?.Invoke(downloadSuccessEventArgs.DownloadPath);
            }    
        });
        GameEntry.Event.Subscribe(DownloadFailureEventArgs.EventId, (sender, eventArgs) =>
        {
            DownloadFailureEventArgs downloadFailureEventArgs = eventArgs as DownloadFailureEventArgs;
            if (downloadFailureEventArgs.SerialId == downloadId)
            {
                callBack?.Invoke(null);
            }    
        });
    }
}

