﻿using GameFramework;
using GameFramework.FileSystem;
using GameFramework.Resource;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityGameFramework.Runtime;
using XLua;

namespace JMatrix
{
    public class LuaComponent : GameFrameworkComponent
    {
        public LuaEnv luaEnv;
        float lastGCTime = 0;
        float gcInterval = 1;

        [HideInInspector]
        public LuaLooper luaLooper;
        static string luaFileHeadPath = "Assets/GameMain/Scripts/Lua/{0}.lua.txt";
        bool isRun = false;

        public void Run()
        {
            Log.Debug("LuaManger Init:" + System.DateTime.Now.ToString());
            luaEnv = new LuaEnv();

            if (GameEntry.Base.EditorResourceMode)
            {
                luaEnv.AddLoader(EditorLoader);
#if UNITY_EDITOR && EDITOR_HOTRELOAD//放一些有关Lua端调试的代码
                luaEnv.DoString("require('EditorMain')");
#endif
            }
            else
            {
#if UNITY_EDITOR && USE_LUA_SOURCE
                luaEnv.AddLoader(EditorLoader);
#else
                luaEnv.AddLoader(CustomLoader);
#endif
            }
            float start = Time.realtimeSinceStartup;
            LoadLuaTime = 0;
            luaEnv.DoString("require('Main')");
            float end = Time.realtimeSinceStartup;
            Debug.LogWarningFormat("require Lua Time:{0} Load:{1}", end - start, LoadLuaTime);
            InitLuaLooper();
        }
        public static float LoadLuaTime = 0;

        byte[] EditorLoader(ref string filePath, ref int length)
        {
            float start = Time.realtimeSinceStartup;
            string realFilePath = filePath.Replace('.', '/');
            string realPath = Utility.Text.Format(luaFileHeadPath, realFilePath);
            string path = Application.dataPath.Replace("/Assets", "/") + realPath;
            if (!File.Exists(path))
            {
                return null;
            }
            byte[] content = File.ReadAllBytes(path);
            // Log.Info("EditorLoader============={0}", path);
            filePath = path;
            length = content.Length;
            LoadLuaTime += Time.realtimeSinceStartup - start;
            return content;
        }

        byte[] readBuffer = new byte[1024 * 512];
        byte[] CustomLoader(ref string filePath, ref int length)
        {
            float start = Time.realtimeSinceStartup;
            string realFilePath = filePath.Replace('.', '/');
            string realPath = Utility.Text.Format(luaFileHeadPath, realFilePath);
            byte[] bytes = GameEntry.Resource.LoadBinaryFromFileSystem(realPath);
            length = bytes.Length;
            LoadLuaTime += Time.realtimeSinceStartup - start;
            return bytes;
        }

        void InitLuaLooper()
        {
            lastGCTime = Time.time;
            isRun = true;

            if (luaLooper == null)
            {
                GameObject loop = new GameObject("LuaLooper");
                luaLooper = loop.AddComponent<LuaLooper>();
                luaLooper.Init();
                loop.transform.SetParent(transform);
            }
        }
        void Update()
        {
            if (isRun)
            {
                if (Time.time - lastGCTime > gcInterval)
                {
                    luaEnv.Tick();
                    lastGCTime = Time.time;
                }
            }
        }

        public void Dispose()
        {
            isRun = false;
            if (luaEnv != null)
            {
                luaEnv.Dispose();
                luaEnv = null;
            }

            if (luaLooper != null)
            {
                GameObject.Destroy(luaLooper.gameObject);
            }
            luaLooper = null;
        }
    }
}