return {
    id = 10007,
    name = "中秋节发饰",
    desc = "中秋节发饰",
    price = 200,
    upgrade_to_item_id = 10008,
    expire_time = 2021-10-10 23:59:59,
    batch_useable = false,
}