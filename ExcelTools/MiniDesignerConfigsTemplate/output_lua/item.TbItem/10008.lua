return {
    id = 10008,
    name = "中秋节鞋子",
    desc = "中秋节鞋子",
    price = 300,
    upgrade_to_item_id = 10009,
    expire_time = 2021-10-11 23:59:59,
    batch_useable = false,
}